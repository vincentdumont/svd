#!/usr/bin/env python2
import os,argparse,time

# Input arguments
parser = argparse.ArgumentParser(prog='download',description='Download video segments and join.')
parser.add_argument('remote', help='Remote path location')
parser.add_argument('--quality',metavar='',type=int,help='Quality index')
parser.add_argument('--proxy',metavar='',help='Proxy link')
parser.add_argument('--maxseg',metavar='',default=220,help='Maximum number of segments')
parser.add_argument('--token',metavar='',help='Token string')
parser.add_argument('--format',metavar='',default=720,help='Pixel format resolution')
args = parser.parse_args()

# Initialize string value to join segment files
joinpath=''

# Loog over segments
for nseg in range(1,args.maxseg):

    # Define filename based on given arguments
    if args.quality!=None:
        filename='segment%i_%i_av.ts'%(nseg,args.quality)
    else:
        filename='seg-%i-v1-a1.ts'%nseg
    print '%s ...'%filename

    # Download file if not already done
    if os.path.exists(filename)==False:

        # Define wget command
        command='wget -o wget-log '
        if args.proxy!=None:
            command+='-e https_proxy=%s %s '%args.proxy
        command+='%s/%s'%(args.remote,filename)
        if args.token!=None:
            command+='?q=%i&token=%s'%(args.format,args.token)

        # Run wget
        os.system(command)

        # Wait for download to complete and check for error message
        time.sleep(5)
        while 'saved' not in open('wget-log').read():
            if 'ERROR' in open('wget-log').read():
                print 'File not found.'
                quit()

        # Rename video file and delete log
        os.system('mv %s*%i %s'%(filename,args.format,filename))
        os.system('rm wget-log')

    joinpath+=filename+' '
    print '\tdone!'
        
os.system('cat %s > complet.ts'%joinpath)
os.system('ffmpeg -i complet.ts -acodec copy -vcodec copy complet.mp4')
